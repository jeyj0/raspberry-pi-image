.PHONY: image

image:
	nixos-generate -f sd-aarch64-installer --system aarch64-linux -c sd-image.nix -I nixpkgs=./nixpkgs
