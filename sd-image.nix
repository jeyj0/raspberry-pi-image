{ lib, pkgs, ... }:
let
	user = "nixos";
in
{
	imports = [
		<nixpkgs/nixos/modules/installer/cd-dvd/sd-image-aarch64.nix>
	];

	sdImage.compressImage = false;

	# fileSystems."/home/${user}/data-drive".device = "/dev/sda1";
	fileSystems."/data-drive".device = "/dev/sda1";

	users.groups."${user}" = {};
	users.users."${user}" = {
		shell = pkgs.fish;
		openssh.authorizedKeys.keys = [
			"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCfAQJbrL2m3Aw+xGECGYvUaozS4P5DFhXSZDyh0ixTHeZSXffSrqdwTmGFc5ZISr85ABqqDXVnawFfd6zxZiC0FK4hLFmH7YtpBwevRwJ6bn0t7GcqfCZd+g1r3xgYiw4PKcIdpFE9DVqyuiKK+ryughkfjoc+uQBqIAc1TuCAGPB6oyU+IslWtXCnQepW67V2qLu82NSEP2+NP7CvRszkvf85GrPAsElyCfA0as4AztMo1ms+lf3FwKzpYChn/iWpSpp+3SizWJ2ypcDNNnUV0YMV/Lgyw87xIcM2Qlh8+4NqmlFb1nKINucFBztiEPX93urMg2I6qF9rt5bcNk/3Ii6OsDji72jxFKFukxWx7Sa/8XVmV1MYAgvM77jKajNYEXoZyZFzkkzxy6/fkPRJzwZ4RHBSBi8jWxdTMV5pWSmsWir4kW/bbuw/oQ903cms81fLO1djdl3ZUoQ7btP2bkUHFQcNHBVXuwiS74Gz54xL6Vs1zdS0+PL4KJLtCpbtlZDUeZr025PzlKLgHZnPmnUFrzxcqcwTaK4et70da1BRtqe8GZ+4CJWKMRPLzDklGSmVXpZ+w6P/MolxsQnUL9OudnjX26le6m2aRs5fJBHBS1uf+e/+LgfY17z9j8Kx9hlKT18oe4reO/j0istTKzbW07lDxLg8zF5wq6Oh3Q== jannis@jorre.dev"
		];
		createHome = true;
		group = "${user}";
	};

	systemd.services.sshd.wantedBy = lib.mkOverride 40 [ "multi-user.target" ];

	networking.firewall.enable = true;
	networking.firewall.allowedTCPPorts = [ 8384 ];

	services = {
		openssh.enable = true;
		syncthing = {
			enable = true;
			openDefaultPorts = true;
			guiAddress = "0.0.0.0:8384";
			dataDir = "/data-drive/syncthing";
			configDir = "/data-drive/syncthing/.config/syncthing";
		};
		cron = {
			enable = true;
			systemCronJobs = [
				"0 22 * * * shutdown now"
			];
		};
	};

	environment.systemPackages = with pkgs; [
		fish
		git
		htop
	];
}
