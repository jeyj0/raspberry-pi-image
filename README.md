# Raspberry Pi Image

Using the code in this project it should be possible to create a fully-configured raspberry pi image to flash on an SD card and use out-of-the-box for all my use-cases, without doing any further installations or work on the Pi itself.

## Steps

(all commands are assumed to be run from the project directory)

1. Be on a NixOS system, using a channel where the qemu version is >=5.0.0
2. Add `boot.binfmt.emulatedSystems = [ "aarch64-linux" ];` to the system configuration
3. Run `sudo nixos-rebuild switch` to install all dependencies on the build machine
4. Install `nixos-generators` using `nix-env -f https://github.com/nix-community/nixos-generators/archive/master.tar.gz -i`
5. Clone nixpkgs into this project folder using `git clone --depth=1 -b release-20.09 https://github.com/NixOS/nixpkgs`
6. Build the image using `nixos-generate -f sd-aarch64-installer --system aarch64-linux -c sd-image.nix -I nixpkgs=./nixpkgs`
7. The last line of the output above is the path of a `.img` file, which is ready to be flashed on an SD card for the Pi to run off of.

## Sources

- [Custom NixOS build for Raspberry Pis](https://rbf.dev/blog/2020/05/custom-nixos-build-for-raspberry-pis/#building-on-nixos-using-nixos-generators)
